const express = require('express');
const router = express.Router();
const serverController = require('../controller/ServerController');
const authMid = require('../service/auth.midware');

router.use(authMid.setCurrentUser);

router.get('/', authMid.isHaveLoggedIn, serverController.findServer);

module.exports = router;