const express = require('express');
const router = express.Router();
const serverController = require('../controller/ServerController');

router.post('/', serverController.listChannel);

module.exports = router;