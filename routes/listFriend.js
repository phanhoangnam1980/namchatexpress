const express = require('express');
const router = express.Router();
const friendController = require('../controller/FriendController')
const authMid = require('../service/auth.midware');

router.use(authMid.setCurrentUser);

router.get('/', authMid.isHaveLoggedIn, friendController.listFriends);

module.exports = router;