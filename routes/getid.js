const express = require('express');
const router = express.Router();
const userController = require('../controller/UserController');
const authMid = require('../service/auth.midware');

router.use(authMid.setCurrentUser);

router.get('/', authMid.isHaveLoggedIn, userController.getIdFriend);

module.exports = router;