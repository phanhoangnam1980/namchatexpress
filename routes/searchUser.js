const express = require('express');
const router = express.Router();
const searchController = require('../controller/SearchController');
const authMid = require('../service/auth.midware');
router.use(authMid.setCurrentUser);

router.get('/', authMid.isHaveLoggedIn, searchController.SearchResult);

module.exports = router;