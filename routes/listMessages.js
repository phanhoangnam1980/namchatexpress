const express = require('express');
const router = express.Router();
const MessagesController = require('../controller/MessagesController');
const authMid = require('../service/auth.midware');

router.use(authMid.setCurrentUser);

router.post('/', authMid.isHaveLoggedIn, MessagesController.listMessages);

module.exports = router;