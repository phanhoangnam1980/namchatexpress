const database = require('../Database/database');
const messageUnit = require('../service/messageUnits.service')

exports.mappingChannel = async (serverId, currentUser) => {
    const check = await messageUnit.findMatchUserServer(serverId, currentUser);
    if (!check.length) {
        await messageUnit.save(0, serverId, currentUser);
    } else {
        await messageUnit.updateMessageUnits(0, currentUser[0].id, serverId);
    }
}

exports.save = async (nameChannel, ServerId) => {
    const sortid = database.select('*').from('Channel').size +
        (database.select('*').from('Groups').size / 2) +
        database.select('*').from('Server').size + 1;
    const channel = {namechannel: nameChannel, serverid: ServerId, channelid: sortid}
    database('chanel_server').insert(channel);
    return channel;
}

exports.findChannel_in_Server = async (serverId) => {
    return database.from('chanel_server').where({serverid: serverId})
}