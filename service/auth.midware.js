const jwt = require('jsonwebtoken');
const database = require('../Database/database');
const application = require('../utils/application.utils');

exports.setCurrentUser = function (req, res, next) {
    const authorization = req.header("Authorization");
    const token = authorization.replace('Bearer ', '');
    const data = jwt.verify(token, application.secret, {algorithm: 'HS512'});
    database.from('user').where({email: data.email}).then(user => {
        req.user = user;
        next();
    });
}

exports.isHaveLoggedIn = function (req, res, next) {
    if (req.user) {
        next();
    } else {
        res.send(401, 'Unauthorized');
    }
}