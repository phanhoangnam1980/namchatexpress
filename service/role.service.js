const database = require('../Database/database');

exports.saveNewUserServer = function (serverId, userId) {
    let roleId;
    database('Custom_Role').insert({role: 'admin', serverid: 'serverId'}).then(data => {
        roleId = data[0].id
    });
    database('Custom_Role').insert({role: 'everyone', serverid: 'serverId'});
    database('Control_User_Role').insert({userid: userId, rolecustom: roleId, serverid: serverId});
    database('user_role_server').insert({userid: userId, serverid: serverId, customroles: 1});
}