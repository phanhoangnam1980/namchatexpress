const database = require('../Database/database');

exports.findByUser = async (currentUser) => {
    const listServer = await database.from('User_Role_Server').where({userid: currentUser[0].id});
    const serverUser = []
    const totalListServer = listServer.length;
    for (let i = 0; i < totalListServer; i++) {
        const dataServer = await database.from('server_role').where({serverid: listServer[i].serverid});
        serverUser.push(dataServer[0]);
    }
    return serverUser;
}

exports.findByNameServer = async (ServerName) => {
    return database.from('Server').where('nameserver', 'like', '%' + ServerName + '%');
}