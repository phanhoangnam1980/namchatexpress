const database = require('../Database/database');

exports.getMessageOutput = function (messages, numberMessageSort) {
    messages = messages.filter((messagesResult) => {
        return messagesResult.sortnumber > messages.length - numberMessageSort - 10;
    })
    return messages;
}

exports.nextMessageOut = function (messages, numberMessageSort) {
    messages = messages.filter((messagesResult) => {
        return messagesResult.sortnumber > messages.length - numberMessageSort - 10 &&
            messagesResult.sortnumber < messages.length - numberMessageSort;
    })
    return messages;
}

exports.setMessages = async (messages) => {
    const data = JSON.parse(messages);
    const number = await database.select('*').from("messages").where({groupid: data.groupid});
    data.datetime = new Date().toString().replace(/T/, ':').replace(/\.\w*/, '');
    data.sortnumber = number.length;
    database("messages").insert(data);
    return data;
}

exports.saveMessages = function (data) {
    database("messages").insert(data);
}