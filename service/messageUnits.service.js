const database = require('../Database/database');

exports.findMatchUserServer = async (serverId, currentUser) => {
    return database.from('Message_Units').where({userid: currentUser[0].id, serverid: serverId});
}

exports.updateMessageUnits = async (message, currentid, serverId) => {
    return database.from('Message_Units').where({userid: currentid, serverid: serverId}).update({messages: message});
}

exports.findByGroupId = async (groupid) => {
    return database.from('Message_Units').where({serverid: groupid});
}

exports.save = async (numberMessage, serverId, currentUser) => {
    return database('Message_Units').insert({userid: currentUser[0].id, messages: numberMessage, serverid: serverId})
}