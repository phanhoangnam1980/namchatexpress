const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const routeSign = require('./routes/signin');
const listServer = require('./routes/listserver');
const ListFriend = require('./routes/listFriend');
const listChannels = require('./routes/listchannel');
const mappingChannels = require('./routes/mappingChannel');
const creatNewServer = require('./routes/creatnewserver');
const findServer = require('./routes/findServer');
const getid = require('./routes/getid');
const searchUser = require('./routes/searchUser');
const listMessages = require('./routes/listMessages');
const FriendDetail = require('./routes/FriendDetail');
const listNextMessages = require('./routes/listnextMessages');
const {setMessages} = require("./service/message.service");

const app = express();
const server = require('http').createServer(app)
const io = require('socket.io').listen(server);

// view engine setup
const corsOptions = {
    origin: 'http://localhost:4200',
    method: 'GET, POST, PUT, DELETE,OPTIONS',
    allowedHeaders: 'Authorization, Cache-Control, Content-Type, Access-Control-Allow-Origin',
    credentials: true,
}

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
app.use('/signin', routeSign);
app.use('/listserver', listServer);
app.use('/listFriend', ListFriend);
app.use('/listchannel', listChannels);
app.use('/mappingChannel', mappingChannels);
app.use('/creatnewserver', creatNewServer);
app.use('/findServer', findServer);
app.use('/getid', getid);
app.use('/searchUser', searchUser);
app.use('/listMessages', listMessages);
app.use('/FriendDetail', FriendDetail);
app.use('/listnextMessages', listNextMessages);

const SocketIo = io.of('/wsocket');

//app.use(express.static("public"));

SocketIo.on('connection', socket => {
    console.log("socketIo running");

    socket.on('sendMessage', data => {
        const dataResult = setMessages(data)
        SocketIo.emit('message', dataResult);
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });

    socket.on("typing", data => {
        socket.broadcast.emit("typing", data);
    });

    socket.on('timeoutTyping', () => {
        socket.broadcast.emit('timeout');
    })

    socket.on('sendImage', () => {
        socket.broadcast.emit('confirm');
    })
});

server.listen(5000, () => {
    console.log("socket io started on port 5000");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


module.exports = app;
