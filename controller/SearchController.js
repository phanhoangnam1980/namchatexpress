const database = require('../Database/database');

exports.SearchResult = async (req, res) => {
    const currentUser = req.user;
    const emails = req.body;
    const data = database.from('User').where('email', 'like', '%' + emails + '%');
    const dataLength = data.length;
    for (let i = 0; i < dataLength; i++) {
        if (data[i].email.equal(currentUser.email)) {
            data.splice(i, 1);
        }
    }
    res.json({result:data})
}