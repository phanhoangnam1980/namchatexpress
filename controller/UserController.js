const bcrypt = require('bcrypt');
const database = require('../Database/database');
const jwt = require('jsonwebtoken');
const application = require('../utils/application.utils')

exports.signInJwt = async (req, res) => {
    const {email, password} = req.body;
    if (!email) res.throw(401, 'Email required');
    if (!password) res.throw(401, 'Password required');

    const DataUser = await database.from('user').where({email});

    if (await bcrypt.compare(password, DataUser[0].password)) {
        const token = jwt.sign({email: DataUser[0].email}, application.secret, {algorithm: 'HS512'});
        return res.json({accessToken: token, tokenType: 'Bearer'});
    }
    return res.status(500);
}

exports.signUpJwt = async (req, res) => {
    const {email, password, passwordConfirm} = req.body;
    if (!email) res.throw(401, 'Email required');
    if (!password) res.throw(401, 'Password required');
    if (!passwordConfirm) res.throw(401, 'PasswordConfirmed required');

    database.from('user');
}

exports.getIdFriend = async (req, res) => {
    const getid = req.body;
    const UserId = req.user.userid
    const findfrienderror = await database.from('Friend').where({friendid: getid, userid: UserId}, function(err,user){
        if (err) {
            res.json(500, err);
        } else if (!user) {
            res.json(404, {reason: "No such username", username: username});
        } else {
            res.json(200, user);
        }
    });
    if (findfrienderror.size < 1) {
        database('Friend').insert({userid: UserId, friendid: getid, groupid: 0});
    }
    const DataUser = await database.from('User').where({id: getid});
    res.json({result: DataUser[0]});
}