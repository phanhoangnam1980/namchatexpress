const serverService = require('../service/Server.service');
const channelService = require('../service/channel.service')
const roleService = require('../service/role.service')

exports.listServer = async (req, res) => {
    const data = await serverService.findByUser(req.user);
    return res.json({servers: data, member: [], userserver: []});
}

exports.listChannel = async (req, res) => {
    const {serverid} = req.body;
    const data = await channelService.findChannel_in_Server(serverid);
    return res.json({channel: data});
}

exports.mappingChannel = async (req, res) => {
    const {serverid} = req.body;
    await channelService.mappingChannel(serverid, req.user);
    res.json({result: 'in'});
}

exports.creatNewChannel = async (req, res) => {
    const {namechannel, serverid} = req.body;
    const data = await channelService.save(namechannel, serverid);
    res.json({channel: data[0]})
}

exports.setServer = async (req, res) => {

}

exports.findServer = async (req, res) => {
    const {servername} = req.body;
    const serverFound = serverService.findByNameServer(servername);
    const currentUser = req.user;
    roleService.saveNewUserServer(serverFound[0].serverid, currentUser.id);
    res.json({servers: serverFound});
}

