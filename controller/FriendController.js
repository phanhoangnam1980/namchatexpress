const jwt = require('jsonwebtoken')
const database = require('../Database/database');
const application = require('../utils/application.utils')

exports.listFriends = async (req, res) => {
    const User = req.user;
    const DataFriend = [];
    const friendList = await database.from('friend').where({userid: User[0].id});
    const totalFriend = friendList.length;
    for (let i = 0; i < totalFriend; i++) {
        const users = await database.from('user').where({id: friendList[i].friendid});
        DataFriend.push({friendresult: friendList[i], result: users[0]});
    }
    res.json(DataFriend);
}
