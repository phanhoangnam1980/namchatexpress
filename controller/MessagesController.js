const database = require('../Database/database');
const MessageUnitService = require('../service/messageUnits.service');
const MessageService = require('../service/message.service')

exports.listMessages = async (req, res) => {
    const {groups} = req.body;
    let messages = await database.from('messages').where({groupid: groups});
    const check = await MessageUnitService.findByGroupId(groups);
    if (!check.length) {
        await MessageUnitService.save(0, groups, req.user);
        messages = MessageService.getMessageOutput(messages, 0);
    } else {
        messages = MessageService.getMessageOutput(messages, 0);
    }
    res.json({messagesresult: messages});
}

exports.getFriendDetail = async (req, res) => {
    const {friendId} = req.body;
    const friendUser = await database('user').where('id', friendId);
    res.json({result: friendUser[0]});
}

exports.nextMessages = async (req, res) => {
    const {groups} = req.body;
    let messages = await database.from('messages').where({groupid: groups});
    let check = await MessageUnitService.findMatchUserServer(groups, req.user);
    if (messages.length > (check[0].messages + 10)) {
        await MessageUnitService.updateMessageUnits(check[0].messages + 10, req.user, groups);
    } else {
        await MessageUnitService.updateMessageUnits(messages.length, req.user, groups);
    }
    check = await MessageUnitService.findMatchUserServer(groups, req.user);
    MessageService.nextMessageOut(messages,check[0].messages);
    res.json({messagesresult: messages});
}