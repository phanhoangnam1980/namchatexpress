exports.secret = process.env.JWT_Secret || 'JWTSuperSecretKey';
exports.jwtExpirationInMs = 604800000;